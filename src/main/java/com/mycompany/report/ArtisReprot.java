/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class ArtisReprot {

    private int id;
    private String name;
    private int totalQty;
    private float totalPrice;

    public ArtisReprot(int id, String name, int totalQty, float totalPrice) {
        this.id = id;
        this.name = name;
        this.totalQty = totalQty;
        this.totalPrice = totalPrice;
    }

    public ArtisReprot(String name, int totalQty, float totalPrice) {
        this.id = -1;
        this.name = name;
        this.totalQty = totalQty;
        this.totalPrice = totalPrice;
    }

    public ArtisReprot() {
        this(-1, "", 0, 0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "ArtisReprot{" + "id=" + id + ", name=" + name + ", totalQty=" + totalQty + ", totalPrice=" + totalPrice + '}';
    }

    public static ArtisReprot fromRS(ResultSet rs) {
        ArtisReprot obj = new ArtisReprot();
        try {
            obj.setId(rs.getInt("ArtistId"));
            obj.setName(rs.getString("Name"));
            obj.setTotalQty(rs.getInt("QTY"));
            obj.setTotalPrice(rs.getInt("Total"));

        } catch (SQLException ex) {
            Logger.getLogger(ArtisReprot.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }

}
