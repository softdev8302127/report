/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class ArtistDao implements Dao<Artis> {

    @Override
    public Artis get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Artis> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Artis save(Artis obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Artis update(Artis obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Artis obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Artis> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<ArtisReprot> getArtistByTotalPrice(int limit) {
        ArrayList<ArtisReprot> list = new ArrayList();
        String sql = """
                     SELECT art.*,SUM(ini.Quantity)as QTY ,SUM(ini.UnitPrice * ini.Quantity) as Total  
                      FROM artists art
                     INNER JOIN albums alb ON alb.AlbumId = art.ArtistId
                     INNER JOIN tracks tra ON tra.AlbumId=alb.AlbumId
                     INNER JOIN invoice_items ini ON ini.TrackId = tra.TrackId
                     INNER JOIN invoices inv ON inv.InvoiceId = ini.InvoiceId
                        /* AND strftime('%Y',inv.InvoiceDate) = "2010"*/
                     GROUP BY art.ArtistId
                     ORDER BY Total DESC
                     LIMIT ?""";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ArtisReprot obj = ArtisReprot.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ArtisReprot> getArtistByTotalPrice(String begin, String end, int limit) {
        ArrayList<ArtisReprot> list = new ArrayList();
        String sql = """
                     SELECT art.*,SUM(ini.Quantity)as QTY ,SUM(ini.UnitPrice * ini.Quantity) as Total  
                      FROM artists art
                     INNER JOIN albums alb ON alb.AlbumId = art.ArtistId
                     INNER JOIN tracks tra ON tra.AlbumId=alb.AlbumId
                     INNER JOIN invoice_items ini ON ini.TrackId = tra.TrackId
                     INNER JOIN invoices inv ON inv.InvoiceId = ini.InvoiceId
                        AND inv.InvoiceDate BETWEEN ? AND ?
                     GROUP BY art.ArtistId
                     ORDER BY Total DESC
                     LIMIT  ?""";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ArtisReprot obj = ArtisReprot.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
