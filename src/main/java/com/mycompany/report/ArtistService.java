/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.report;

import java.util.List;

/**
 *
 * @author User
 */
public class ArtistService {

    public List<ArtisReprot> getTopTenArtistByTotalPrice() {
        ArtistDao dao = new ArtistDao();
        return dao.getArtistByTotalPrice(10);
    }
     public List<ArtisReprot> getTopTenArtistByTotalPrice(String begin ,String end) {
        ArtistDao dao = new ArtistDao();
        return dao.getArtistByTotalPrice(begin,end,10);
    }
}
